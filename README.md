# 开放平台数据接口调用最简示例

## 一、示例框架介绍

```
main/java/com/yonyou/openapi
├── common              # 核心基础包
│   │── cryptor         # 生成签名类
│   │── event           # 事件订阅相关
│   ├── exception       # 异常处理
├── config              # 配置类
├── controller          # 事件订阅、税务云回调服务样例  
├── util		        # 工具类
test/java/com/yonyou/openapi
├── isvdemo             # 服务商demo
├── selfbuidlddemo      # 自建应用demo
```

## 二、以下为配置文件信息，使用时需要将授权key、密钥改为您自己创建的。

```properties

# 公有云模式获取租户所在数据中心域名
open-api.gateway-address-url=https://apigateway.yonyoucloud.com/open-auth/dataCenter/getGatewayAddress?tenantId=%s

#服务商授权key信息(生态项目使用)
open-api.app-key=d5d7f8d28d******0b51a261e7b
open-api.app-secret=0c39bdb64e2******05f6d67d19f440911957d


#自建授权key信息(客开项目使用)
open-api.app-key=0724abf725204******bb6ac525c95fc
open-api.app-secret=4d38b636fc1******09dfa05e577f57c651e95

```

## 三、运行示例前需要修改租户id

![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240626091750.png)

## 四、改完配置文件内容，找到示例直接运行即可

| 场景 |  代码位置 |
| --- |  --- | 
| 自建应用调用接口示例 | com.yonyou.openapi.isvdemo.isvdemo.java |
| 服务商调用接口示例| com.yonyou.openapi.selfbuilddemo.SelfBuildDemo.java |
| 税务云回调服务样例 |  com.yonyou.openapi.controller.TaxCallbackController.java | 
| 事件订阅回调服务样例 |  com.yonyou.openapi.controller.EventListenerController.java |





