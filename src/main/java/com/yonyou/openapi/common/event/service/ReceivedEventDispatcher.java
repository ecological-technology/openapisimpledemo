package com.yonyou.openapi.common.event.service;

import com.yonyou.openapi.common.event.EventListener;
import com.yonyou.openapi.common.event.cryptor.EncryptionHolder;
import com.yonyou.openapi.common.event.pojo.Event;

import java.util.List;

/**
 * @author nishch
 * @description: TODO
 * @date 2023/8/2
 */
public interface ReceivedEventDispatcher {

    @SuppressWarnings("rawtypes")
    List<EventListener> findEventListens();

    void dispatch(Event event, String source, EncryptionHolder holder);
}
