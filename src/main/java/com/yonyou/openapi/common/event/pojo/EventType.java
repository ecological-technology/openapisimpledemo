package com.yonyou.openapi.common.event.pojo;

/**
 * @author nishch
 * @description: 事件编码
 * @date 2023/8/2
 */
public class EventType {

    /**
     * 部门创建
     */
    public static final String DEPT_ADD = "DEPT_ADD";
}
