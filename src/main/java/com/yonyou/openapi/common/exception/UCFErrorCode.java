package com.yonyou.openapi.common.exception;


public class UCFErrorCode {


    public static final Integer ILLEGAL_ARGUMENT = 40010;

    public static final Integer ILLEGAL_ARGUMENT_AI_FACE = 40012;

    public static final Integer UNSUPPORTED_OPERATION = 40060;

    public static final Integer AUTHENTICATION_FAILED = 40101;

    public static final Integer UNAUTHORIZED = 40301;

    public static final Integer ILLEGAL_STATE = 40310;

    public static final Integer RATE_LIMIT_REACHED = 40311;

    public static final Integer RESOURCE_NOT_FOUND = 40401;

    public static final Integer RESOURCE_CONFLICT = 40901;

    public static final Integer REMOTE_SERVICE = 50050;

    public static final Integer IO_ERROR = 50060;

    public static final Integer INTERRUPTED = 50080;

    public static final Integer DATA_PERSISTENT_ERROR = 50090;

    public static final Integer SERVER_CONFIGURATION_ERROR = 50070;


}
