package com.yonyou.openapi.controller;

import com.alibaba.fastjson.JSONObject;
import com.yonyou.openapi.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description: 回调接口写法，标准的restful风格，http接口即可。url、delurl写法都一样。
 * @Author: jiaoguojin
 * @Date: 2022/6/9 15:38
 */
@RestController
@Slf4j
@RequestMapping("/kaipiao")
public class TaxCallbackController {

    /**
     * 功能描述: 第一种拿map接收
     * 返回json结构参考文档地址：https://fapiao.yonyoucloud.com/apidoc/api/%E9%94%80%E9%A1%B9-%E5%BC%80%E7%A5%A8%E7%94%B3%E8%AF%B7.html#%E9%99%84%E5%BD%95
     * @Author: jiaoguojin
     * @Date: 2022/6/9 16:53
     */
    @ResponseBody
    @RequestMapping("/callback")
    public Result callback(@RequestBody Map<String,Object> map){
        log.info(map.toString());
        Result result = new Result();
        result.setCode("0000");
        result.setMsg("调用成功");
        return result;
    }

    /**
     * 功能描述: 第二种拿json介绍，注意必须是阿里巴巴json
     * 返回json结构参考文档地址：https://fapiao.yonyoucloud.com/apidoc/api/%E9%94%80%E9%A1%B9-%E5%BC%80%E7%A5%A8%E7%94%B3%E8%AF%B7.html#%E9%99%84%E5%BD%95
     * @Author: jiaoguojin
     * @Date: 2022/6/9 16:54
     */
    @ResponseBody
    @RequestMapping("/callback1")
    public Result callback(@RequestBody JSONObject json){
        log.info(json.toString());
        Result result = new Result();
        result.setCode("0000");
        result.setMsg("调用成功");
        return result;
    }
}
