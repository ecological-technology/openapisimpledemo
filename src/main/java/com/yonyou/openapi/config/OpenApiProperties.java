package com.yonyou.openapi.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties(prefix = "open-api")
public class OpenApiProperties{

    private String gatewayAddressUrl;

    private String appKey;

    private String appSecret;

}
