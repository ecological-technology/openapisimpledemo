package com.yonyou.openapi.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RequestUtil {

    private static final String HEADER_CONTENT_JSON = "application/json";

    private static final String DEFAULT_CHARSET = "UTF-8";

    private static PoolingHttpClientConnectionManager cm = null;

    private static CloseableHttpClient httpClient;

    /**
     * 记录开放平台请求结果
     */
    public static class Response {
        /**
         * 该请求的 http 状态码
         * 200 为正常的返回结果
         */
        private int status;

        /**
         * 请求返回消息
         * 当 status == 200 时会返回 response body 中的字符串
         * 当 status !== 200 时会返回具体的错误信息
         */
        private String result;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
    }

    static{
        cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(500);
        cm.setDefaultMaxPerRoute(50);

        RequestConfig globalConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(10000)         // 连接池获取连接超时
                .setConnectTimeout(10000)                   // 连接建立超时
                .setSocketTimeout(30000)                    // 等待响应超时
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES)
                .build();
        APIHttpRequestRetryHandler apiHttpRequestRetryHandler = new APIHttpRequestRetryHandler();
        httpClient = HttpClients.custom().setConnectionManager(cm)
                .setRetryHandler(apiHttpRequestRetryHandler)
                .setDefaultRequestConfig(globalConfig).build();
    }

    private static CloseableHttpClient getHttpClient(){
        return httpClient;
    }

    public static <T> T doGet(String requestUrl, Map<String, String> paramMap, Class<T> type) throws IOException {
        return JSONObject.parseObject(doGet(requestUrl, paramMap), type);
    }

    public static <T> T doGetType(String requestUrl, Class<T> type) throws IOException {
        return JSONObject.parseObject(doGet(requestUrl, null), type);
    }

    public static <T> T doPost(String requestUrl, Object params, Class<T> type) throws IOException {
        return JSONObject.parseObject(doPost(requestUrl, params), type);
    }

    public static String doGet(String requestUrl, Map<String, String> paramMap) throws IOException {
        CloseableHttpClient httpClient = getHttpClient();
        StringBuilder param = new StringBuilder();
        if (paramMap != null) {
            param.append("?");
            for(Map.Entry<String, String> entry: paramMap.entrySet()) {
                param.append(entry.getKey());
                param.append("=");
                param.append(entry.getValue());
                param.append("&");
            }
            param.deleteCharAt(param.length() - 1);
        }

        HttpGet get = new HttpGet(requestUrl + param);
        String responseString = httpClient.execute(get, response -> EntityUtils.toString(response.getEntity()));
        get.releaseConnection();
        return responseString;
    }


    public static String doPost(String requestUrl, Object params) throws IOException {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost post = new HttpPost(requestUrl);
        String paramsStr = JSON.toJSONString(params);
        assert paramsStr != null;
        StringEntity stringEntity = new StringEntity(paramsStr, ContentType.APPLICATION_JSON);
        post.setEntity(stringEntity);
        String responseString = httpClient.execute(post, response -> EntityUtils.toString(response.getEntity()));
        post.releaseConnection();
        return responseString;
    }


    public static String doPostByFile(String requestUrl, Map<String, Object> params, File file, String fileKey) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost post = new HttpPost(requestUrl);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                builder.addTextBody(entry.getKey(),String.valueOf(entry.getValue()),ContentType.TEXT_PLAIN);
            }
        }
        if(!StringUtils.isEmpty(fileKey)){
            builder.addBinaryBody(fileKey,file);
        }
        HttpEntity httpEntity = builder.build();
        post.setEntity(httpEntity);
        String responseString = httpClient.execute(post, response -> EntityUtils.toString(response.getEntity()));
        post.releaseConnection();
        return responseString;
    }

    public static <T> T doPostByForm(String requestUrl, Map<String, Object> params, Class<T> type) throws IOException {
        return JSONObject.parseObject(doPostByForm(requestUrl, params), type);
    }

    public static String doPostByForm(String requestUrl, Map<String, Object> params) throws IOException {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost post = new HttpPost(requestUrl);
        if (params != null && params.size() > 0) {
            List<NameValuePair> pairs = new ArrayList<>();
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                NameValuePair pair = new BasicNameValuePair(entry.getKey(), String.valueOf(entry.getValue()));
                pairs.add(pair);
            }
            HttpEntity httpEntity = new UrlEncodedFormEntity(pairs, "UTF-8");
            post.setEntity(httpEntity);
        }
        String responseString = httpClient.execute(post, response -> EntityUtils.toString(response.getEntity()));
        post.releaseConnection();
        return responseString;
    }

}

