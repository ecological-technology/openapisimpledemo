package com.yonyou.openapi.pojo;

/**
 * @Description:
 * @Author: jiaogjin
 * @Date: 2022/5/26 17:15
 */

public class Result {

    private String code;
    private String msg;
    private Object datas;

    public Result() {
    }

    public Result(String code, String msg, Object datas) {
        this.code = code;
        this.msg = msg;
        this.datas = datas;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getDatas() {
        return datas;
    }

    public void setDatas(Object datas) {
        this.datas = datas;
    }
}
