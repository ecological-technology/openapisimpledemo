package com.yonyou.openapi.pojo;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author nishch
 * @description:
 * @date 2022/3/8
 */

@Data
public class GatewayAddressResponse extends OpenApiResponse<GatewayAddressResponse.GatewayAddressDTO> implements Serializable {


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GatewayAddressDTO {

        private String gatewayUrl;
        private String tokenUrl;

    }
}